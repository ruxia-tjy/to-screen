# toScreen
开始开发C++版本，后期Python版本可能不在开发。

将图片从电脑通过串口显示到屏幕上，目前支持单色OLED屏幕。



## 如何使用

### CPP版本

使用Qt5.9.9进行开发，因为使用的是MinGW编译好的OpenCV，所以请使用MInGW编译Qt，如果更改为MSVC请自行编译相应的OpenCV。



### Python版本

发布版本通过pyinstaller打包。再Windows上，您可以直接下载exe程序运行，或者下载源代码，通过命令

```shell
pip install -r requirements.txt
```

安装相关包，运行toScreen.py文件。



## 配置config

详细见doc/config.md文件

### 教程

图像大小为128x64，（128x64） / 8 = 1024。所以在您的单片机中接收1024bytes.

以下为Arduino示例代码，根据您的需要修改：

```
#include<Adafruit_GFX.h>
#include<Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
#define OLED_RESET 10
#define OLED_I2C_ADDRESS 0x3c

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// 存储从电脑上接收到的数据
// 使用的0.96寸OLED 128*64像素，共计8192bit == 1024 bytes
uint8_t img_buffer[1024] = {0};

void setup() {
  Serial.begin(115200);
  display.begin(SSD1306_SWITCHCAPVCC, OLED_I2C_ADDRESS);
  display.clearDisplay();
  
  display.display();
}


void loop() {
  while (Serial.available() > 0) {
    display.clearDisplay();
    // 读取1024字节
    Serial.readBytes(img_buffer, 1024);
    // 绘图
    display.drawBitmap(0, 0, img_buffer, SCREEN_WIDTH, SCREEN_HEIGHT, 1);
    display.display();
  }
}
```
