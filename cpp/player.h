#ifndef PLAYER_H
#define PLAYER_H

#include <QWidget>
#include<QLabel>
#include"opencv2/opencv.hpp"
#include"opencv2/highgui.hpp"
#include<QTimer>
#include<QMenu>
#include<QAction>
#include<QContextMenuEvent>
#include<QDebug>

using namespace cv;

typedef enum{
    FileType_Camera=0,
    FileType_LocalVideo,
    FileType_LocalImage
}FileType;


/// 播放模式
typedef enum{
    PlayMode_Normal=0,
    PlayMode_Loop,
    PlayMode_PlayTwice,
    PlayMode_PlayThreeTimes
} PlayMode;

/**
 * @brief The Player class
 *
 * 播放器
 */
class Player : public QWidget
{
    Q_OBJECT

public:
    explicit Player(QWidget *parent = nullptr);

    // 读取
    void readFromCamera(int capNum);
    void readFromLocalVideo(QString filePath,PlayMode mode=PlayMode_Normal);
    void readFromLocalImage(QString filePath);

    void setPlayer(QLabel* player);     // 设置播放器

    void start(int msec);               // 打开
    void stop();                        // 暂停
    void close();                       // 关闭

    // 获取当前的视频类型
    FileType getType();

public:

    int ScreenWidth = 128;
    int ScreenHeight = 64;

    // 播放器
    QLabel *m_player;
    // 菜单
    QMenu *m_contextMenu;
    QAction *m_playAction;
    QAction *m_delAction;

    VideoCapture *m_videoCap;
    QTimer *m_timer;

public:
    void mousePressEvent(QMouseEvent *event) override;


public slots:
    void menu(QAction *action);
    void readFrame();

private:

    FileType type;                      // 文件类型
    QString path = QString("");         // 路径

    Mat image;
};

#endif // PLAYER_H
