#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    player = new Player;
    player->setPlayer(ui->player);

    // 初始化界面
    on_cB_threshold_enable_stateChanged(0);
    on_cB_preview_enable_stateChanged(0);
    // 扫描可用串口
    on_pb_Serial_scan_clicked();
    // 波特率列表
    ui->cB_baud->addItems(serial->standardBaudRates());
}

MainWindow::~MainWindow()
{
    delete ui;
}


/**
 * @brief MainWindow::on_tW_menu_tabBarDoubleClicked
 *
 * 双击显隐目录
 * @param index
 */
void MainWindow::on_tW_menu_tabBarDoubleClicked(int index)
{
    Q_UNUSED(index);
    QPropertyAnimation *pWidgetProcessUp = new QPropertyAnimation(ui->tW_menu,"maximumWidth");
    if(ui->tW_menu->maximumWidth() == 400){
        pWidgetProcessUp->setDuration(300);
        pWidgetProcessUp->setStartValue(400);
        pWidgetProcessUp->setEndValue(20);
        pWidgetProcessUp->setEasingCurve(QEasingCurve::Linear);
        pWidgetProcessUp->start(QAbstractAnimation::DeleteWhenStopped);
    }
    else if(ui->tW_menu->maximumWidth() == 20){
        pWidgetProcessUp->setDuration(300);
        pWidgetProcessUp->setStartValue(20);
        pWidgetProcessUp->setEndValue(400);
        pWidgetProcessUp->setEasingCurve(QEasingCurve::Linear);
        pWidgetProcessUp->start(QAbstractAnimation::DeleteWhenStopped);
    }
}

/**
 * @brief MainWindow::on_pb_Serial_scan_clicked
 *
 * 扫描串口
 */
void MainWindow::on_pb_Serial_scan_clicked()
{
    ui->cB_port->clear();
    ui->cB_port->addItems(serial->scanSerial());
}

/**
 * @brief MainWindow::on_pB_Serial_open_clicked
 * 打开/关闭串口
 */
void MainWindow::on_pB_Serial_open_clicked()
{
    if(ui->pB_Serial_open->text() == tr("打开串口")){
        if(!ui->cB_port->currentText().length()){
            QMessageBox::information(this,"提示","请选择一个串口！");
            return;
        }
        if(serial->open(ui->cB_port->currentText(),
                        ui->cB_baud->currentText().toInt())){
            serial->isOpen = true;
            ui->cB_port->setEnabled(false);
            ui->cB_baud->setEnabled(false);

            ui->pB_Serial_open->setText(tr("关闭串口"));
        }
    }else{
        serial->close();
        serial->isOpen = false;
        ui->cB_baud->setEnabled(true);
        ui->cB_port->setEnabled(true);
        ui->pB_Serial_open->setText(tr("打开串口"));
    }
}

/**
 * @brief MainWindow::on_pB_file_open_clicked
 * 打开文件
 */
void MainWindow::on_pB_file_open_clicked()
{
    if(ui->rB_file_type_Camera->isChecked()){
        player->readFromCamera(0);
    }
}

void MainWindow::on_cB_threshold_enable_stateChanged(int arg1)
{
    switch(arg1){
    case 0:
        // 勾选被取消
        ui->cB_threshold_algorithm->setEnabled(false);
        ui->sB_thresholdValue->setEnabled(false);
        ui->sB_areaValue->setEnabled(false);
        ui->sB_subareaValue->setEnabled(false);
        break;
    case 2:
        ui->cB_threshold_algorithm->setEnabled(true);
        ui->sB_thresholdValue->setEnabled(true);
        ui->sB_areaValue->setEnabled(true);
        ui->sB_subareaValue->setEnabled(true);
        break;
    }
}

void MainWindow::on_cB_preview_enable_stateChanged(int arg1)
{
    switch(arg1){
    case 0:
        ui->rB_preview_Raw->setEnabled(false);
        ui->rB_preview_proccess->setEnabled(false);
        break;

    case 2:
        ui->rB_preview_Raw->setEnabled(true);
        ui->rB_preview_proccess->setEnabled(true);
        break;

    }
}
