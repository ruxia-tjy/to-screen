/*
 * serial.h
 * 创建时间： 2022/5/9
 * 描述: 封装串口通信。
 *
 * 修改时间: 2022/5/23
 * 修改描述:
 *          添加readReady()可修改时长接口
 *
 * 修改时间： 2022/5/9
 * 修改描述：
 *          Qt在大量数据通信时会多次触发readReady()信号，
 *      本文件使用了QTimer在第一次触发readReay()信号时计
 *      时，每次触发的readReay()信号添加到临时缓冲区，当
 *      计时终止时，将临时缓冲区内的内容复制给缓冲区，清空
 *      临时缓冲区。延时时间请根据实际情况进行修改
 */
#ifndef SERIAL_H
#define SERIAL_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QMessageBox>
#include <QTimer>

class Serial:public QObject
{
    Q_OBJECT

public:
    Serial();
    bool isOpen = false;                            // 串口是否已打开
    QStringList scanSerial();                       // 扫描可用串口
    QStringList standardBaudRates();                // 获取标准波特率列表

    bool open(QString serialName, int baudRate,QSerialPort::DataBits databits=QSerialPort::Data8,
            QSerialPort::Parity parity = QSerialPort::NoParity,QSerialPort::StopBits stopBits=QSerialPort::OneStop,
              QSerialPort::FlowControl flowControl=QSerialPort::NoFlowControl);    // 打开串口
    void close();                                   // 关闭串口
    void sendData(QByteArray &sendData);            // 发送数据给下位机
    QByteArray getReadBuf();                        // 获得读取数据缓冲区
    void clearReadBuf();                            // 清除读取数据缓冲区

    void setReadTime(int mesc);                     // 设置读取时长
    int getReadTime();                              // 获取当前读取时长

signals:
    void readSignal();                              // 当下位机中有数据发送过来触发该信号
    void readBufSignal();                           // 计时器终止触发

public slots:
    void readData();                                // 读取数据
    void timerUpdate();

private:
    QSerialPort *m_serialPort;                      // 实例化一个指向串口的指针，可以用于访问串口
    QSerialPortInfo *m_serialPortInfo;              // 实例化QSerialPortInfo
    QByteArray m_tmpBuf;                            // 临时存储下位机发来数据，因为大量数据通信Qt会多次分发
    QByteArray m_readBuf;                           // 返回读取的数据缓冲区
    QTimer *m_timer;                                // 时间对象
    int readTime;                                   // 缓冲区读取时长
};

#endif // SERIAL_H
