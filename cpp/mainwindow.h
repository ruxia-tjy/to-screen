#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<QPropertyAnimation>
#include"serial.h"
#include"player.h"
#include<QDebug>
#include<QException>
#include<QMessageBox>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_tW_menu_tabBarDoubleClicked(int index);
    void on_pb_Serial_scan_clicked();

    void on_pB_Serial_open_clicked();

    void on_pB_file_open_clicked();

    void on_cB_threshold_enable_stateChanged(int arg1);

    void on_cB_preview_enable_stateChanged(int arg1);

private:
    Ui::MainWindow *ui;
    Serial *serial;
    Player *player;

};
#endif // MAINWINDOW_H
