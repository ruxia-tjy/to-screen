'''
    Fun.py
    此文件存储部分函数
'''
from PySide6.QtWidgets import QMessageBox,QInputDialog,QLineEdit

def inputDialog(Mode,Text,TitleText,OKButtonText,CancelButtonText,*value):
    '''
        重写InputDialog,支持中文
        各Mode的value
        0: str(默认值,输入框的模式)
        1: int(默认值，最小值，最大值，步长)
        2: double(默认值，最小值，最大值，步长)
    '''
    if isinstance(Mode,str):
        if Mode.lower() in ['int', '1']:
            Mode = 1
        elif Mode.lower() in ['double','2']:
            Mode = 2
        elif Mode.lower() in ['str',0]:
            Mode = 0
        else:
            return

    dialog = QInputDialog()
    dialog.setInputMode(Mode)
    dialog.setLabelText(Text)
    dialog.setWindowTitle(TitleText)
    dialog.setOkButtonText(OKButtonText)
    dialog.setCancelButtonText(CancelButtonText)

    if Mode == 1:
        dialog.setIntValue(value[0])
        dialog.setIntMinimum(value[1])
        dialog.setIntMaximum(value[2])
        dialog.setIntStep(value[3])
    elif Mode == 2:
        dialog.setDoubleValue(value[0])
        dialog.setDoubleMinimum(value[1])
        dialog.setDoubleMaximum(value[2])
        dialog.setDoubleDecimals(value[3])
    elif Mode == 0:
        dialog.setTextValue(value[0])
        if len(value) == 1:
            dialog.setTextEchoMode(QLineEdit.Normal)
        else:
            if value[1] == 0:
                dialog.setTextEchoMode(QLineEdit.Normal)
            elif value[1] == 1:
                dialog.setTextEchoMode(QLineEdit.NoEcho)
            elif value[1] == 2:
                dialog.setTextEchoMode(QLineEdit.Password)

    if dialog.exec_():
        if Mode == 1:
            return dialog.intValue()
        elif Mode == 2:
            return dialog.doubleValue()
        elif Mode == 0:
            return dialog.textValue()

        return ""

def msgbox(Title,Text,*BtnText):
    '''
        重写MessageBox，支持中文
    '''
    rtxt = 'Y'
    msgbox = QMessageBox()
    msgbox.setWindowTitle(Title)
    msgbox.setText(Text)
    l = len(BtnText)
    if l == 0:
        msgbox.setStandardButtons(QMessageBox.Yes)
        btnY = msgbox.button(QMessageBox.Yes)
        btnY.setText('确定')
        msgbox.exec_()
    elif l == 1:
        msgbox.setStandardButtons(QMessageBox.Yes)
        btnY = msgbox.button(QMessageBox.Yes)
        btnY.setText(BtnText[0])
        msgbox.exec_()
    elif l == 2:
        msgbox.setStandardButtons(QMessageBox.Yes|QMessageBox.No)
        btnY = msgbox.button(QMessageBox.Yes)
        btnY.setText(BtnText[0])
        btnN = msgbox.button(QMessageBox.No)
        btnN.setText(BtnText[1])
        msgbox.exec_()

        if msgbox.clickedButton() == btnN:
            rtxt = 'N'
    elif l == 3:
        msgbox.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
        btnY = msgbox.button(QMessageBox.Yes)
        btnY.setText(BtnText[0])
        btnN = msgbox.button(QMessageBox.No)
        btnN.setText(BtnText[1])
        btnC = msgbox.button(QMessageBox.Cancel)
        btnC.setText(BtnText[2])
        msgbox.exec_()

        if msgbox.clickedButton() == btnN:
            rtxt = 'N'
        elif msgbox.clickedButton() == btnC:
            rtxt = 'C'

    return rtxt
