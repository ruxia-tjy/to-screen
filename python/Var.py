'''
    Var.py
    此文件存储运行时候的部分变量
'''

checkUpdate = False

supportExtension_Video = []
supportExtension_Image = []

default = {
    "baud":115200,
    "tran-width":128,
    "tran-height":64,

    "th":127,
    "side-area":11,
    "sub-area":0,
    "video": {
      "fps": 60,
      "fs": 0,
      "width": 128,
      "height": 64
    },
    "image":{
        "width":128,
        "height":64
    }

}

