# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'MainWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractSpinBox, QApplication, QCheckBox, QComboBox,
    QFrame, QGridLayout, QGroupBox, QHBoxLayout,
    QLabel, QLineEdit, QListWidget, QListWidgetItem,
    QMainWindow, QPushButton, QSizePolicy, QSpinBox,
    QVBoxLayout, QWidget)
import icon_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1030, 586)
        MainWindow.setStyleSheet(u"")
        self.stylesheet = QWidget(MainWindow)
        self.stylesheet.setObjectName(u"stylesheet")
        self.stylesheet.setStyleSheet(u"")
        self.bgApp = QFrame(self.stylesheet)
        self.bgApp.setObjectName(u"bgApp")
        self.bgApp.setGeometry(QRect(5, 5, 1020, 590))
        self.bgApp.setStyleSheet(u"")
        self.bgApp.setFrameShape(QFrame.StyledPanel)
        self.bgApp.setFrameShadow(QFrame.Raised)
        self.lE_filePath = QLineEdit(self.bgApp)
        self.lE_filePath.setObjectName(u"lE_filePath")
        self.lE_filePath.setGeometry(QRect(0, 5, 771, 20))
        self.lE_filePath.setReadOnly(True)
        self.lW_videoinfo = QListWidget(self.bgApp)
        self.lW_videoinfo.setObjectName(u"lW_videoinfo")
        self.lW_videoinfo.setGeometry(QRect(240, 40, 256, 192))
        self.cmdframe = QFrame(self.bgApp)
        self.cmdframe.setObjectName(u"cmdframe")
        self.cmdframe.setGeometry(QRect(0, 20, 216, 566))
        self.cmdframe.setMinimumSize(QSize(151, 566))
        self.cmdframe.setStyleSheet(u"")
        self.cmdframe.setFrameShape(QFrame.StyledPanel)
        self.cmdframe.setFrameShadow(QFrame.Raised)
        self.pB_LoadImageOrVideo = QPushButton(self.cmdframe)
        self.pB_LoadImageOrVideo.setObjectName(u"pB_LoadImageOrVideo")
        self.pB_LoadImageOrVideo.setGeometry(QRect(0, 5, 96, 36))
        self.pB_LoadImageOrVideo.setCursor(QCursor(Qt.PointingHandCursor))
        icon = QIcon()
        icon.addFile(u":/btn/res/\u9009\u62e9\u6587\u4ef6.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pB_LoadImageOrVideo.setIcon(icon)
        self.pB_LoadImageOrVideo.setIconSize(QSize(25, 25))
        self.groupTranserScreen = QGroupBox(self.cmdframe)
        self.groupTranserScreen.setObjectName(u"groupTranserScreen")
        self.groupTranserScreen.setGeometry(QRect(5, 49, 216, 110))
        self.gridLayout_4 = QGridLayout(self.groupTranserScreen)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.label_4 = QLabel(self.groupTranserScreen)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setMaximumSize(QSize(30, 16777215))

        self.gridLayout_4.addWidget(self.label_4, 0, 0, 1, 1)

        self.sB_Width_Transer = QSpinBox(self.groupTranserScreen)
        self.sB_Width_Transer.setObjectName(u"sB_Width_Transer")
        self.sB_Width_Transer.setAlignment(Qt.AlignCenter)
        self.sB_Width_Transer.setMinimum(1)
        self.sB_Width_Transer.setMaximum(1000)
        self.sB_Width_Transer.setValue(128)

        self.gridLayout_4.addWidget(self.sB_Width_Transer, 0, 1, 2, 1)

        self.label_5 = QLabel(self.groupTranserScreen)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setMaximumSize(QSize(30, 16777215))

        self.gridLayout_4.addWidget(self.label_5, 1, 0, 2, 1)

        self.sB_Height_Transer = QSpinBox(self.groupTranserScreen)
        self.sB_Height_Transer.setObjectName(u"sB_Height_Transer")
        self.sB_Height_Transer.setAlignment(Qt.AlignCenter)
        self.sB_Height_Transer.setMinimum(1)
        self.sB_Height_Transer.setMaximum(1000)
        self.sB_Height_Transer.setStepType(QAbstractSpinBox.DefaultStepType)
        self.sB_Height_Transer.setValue(64)

        self.gridLayout_4.addWidget(self.sB_Height_Transer, 2, 1, 1, 1)

        self.cB_fillwhite = QCheckBox(self.groupTranserScreen)
        self.cB_fillwhite.setObjectName(u"cB_fillwhite")

        self.gridLayout_4.addWidget(self.cB_fillwhite, 3, 0, 1, 2)

        self.groupProcess = QGroupBox(self.cmdframe)
        self.groupProcess.setObjectName(u"groupProcess")
        self.groupProcess.setGeometry(QRect(5, 165, 201, 181))
        self.gridLayout_2 = QGridLayout(self.groupProcess)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.sB_Width_Image = QSpinBox(self.groupProcess)
        self.sB_Width_Image.setObjectName(u"sB_Width_Image")
        self.sB_Width_Image.setAlignment(Qt.AlignCenter)
        self.sB_Width_Image.setMinimum(1)
        self.sB_Width_Image.setMaximum(1000)
        self.sB_Width_Image.setValue(128)

        self.gridLayout_2.addWidget(self.sB_Width_Image, 0, 1, 1, 1)

        self.cB_invertColor = QCheckBox(self.groupProcess)
        self.cB_invertColor.setObjectName(u"cB_invertColor")

        self.gridLayout_2.addWidget(self.cB_invertColor, 2, 0, 1, 2)

        self.label_8 = QLabel(self.groupProcess)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setMaximumSize(QSize(30, 16777215))

        self.gridLayout_2.addWidget(self.label_8, 0, 0, 1, 1)

        self.label_9 = QLabel(self.groupProcess)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setMaximumSize(QSize(30, 16777215))

        self.gridLayout_2.addWidget(self.label_9, 1, 0, 1, 1)

        self.sB_Height_Image = QSpinBox(self.groupProcess)
        self.sB_Height_Image.setObjectName(u"sB_Height_Image")
        self.sB_Height_Image.setAlignment(Qt.AlignCenter)
        self.sB_Height_Image.setMaximum(1000)
        self.sB_Height_Image.setValue(64)

        self.gridLayout_2.addWidget(self.sB_Height_Image, 1, 1, 1, 1)

        self.videoFrame = QFrame(self.groupProcess)
        self.videoFrame.setObjectName(u"videoFrame")
        self.gridLayout_3 = QGridLayout(self.videoFrame)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.label_6 = QLabel(self.videoFrame)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setMaximumSize(QSize(30, 16777215))
        self.label_6.setCursor(QCursor(Qt.ArrowCursor))

        self.gridLayout_3.addWidget(self.label_6, 0, 0, 1, 1)

        self.sB_fps = QSpinBox(self.videoFrame)
        self.sB_fps.setObjectName(u"sB_fps")
        self.sB_fps.setAlignment(Qt.AlignCenter)
        self.sB_fps.setMinimum(1)
        self.sB_fps.setMaximum(1000)
        self.sB_fps.setValue(60)

        self.gridLayout_3.addWidget(self.sB_fps, 0, 1, 1, 1)

        self.label_7 = QLabel(self.videoFrame)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setMaximumSize(QSize(30, 16777215))

        self.gridLayout_3.addWidget(self.label_7, 1, 0, 1, 1)

        self.sB_fs = QSpinBox(self.videoFrame)
        self.sB_fs.setObjectName(u"sB_fs")
        self.sB_fs.setAlignment(Qt.AlignCenter)
        self.sB_fs.setMaximum(1000)

        self.gridLayout_3.addWidget(self.sB_fs, 1, 1, 1, 1)


        self.gridLayout_2.addWidget(self.videoFrame, 3, 0, 1, 2)

        self.groupAlgorithm = QGroupBox(self.cmdframe)
        self.groupAlgorithm.setObjectName(u"groupAlgorithm")
        self.groupAlgorithm.setGeometry(QRect(5, 352, 201, 131))
        self.cB_threshold = QComboBox(self.groupAlgorithm)
        self.cB_threshold.addItem("")
        self.cB_threshold.addItem("")
        self.cB_threshold.addItem("")
        self.cB_threshold.addItem("")
        self.cB_threshold.setObjectName(u"cB_threshold")
        self.cB_threshold.setGeometry(QRect(5, 20, 196, 22))
        self.layoutWidget = QWidget(self.groupAlgorithm)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(5, 45, 196, 84))
        self.verticalLayout = QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.lbl_th = QLabel(self.layoutWidget)
        self.lbl_th.setObjectName(u"lbl_th")
        self.lbl_th.setMaximumSize(QSize(60, 16777215))

        self.horizontalLayout.addWidget(self.lbl_th)

        self.sB_thValue = QSpinBox(self.layoutWidget)
        self.sB_thValue.setObjectName(u"sB_thValue")
        self.sB_thValue.setAlignment(Qt.AlignCenter)
        self.sB_thValue.setMaximum(255)
        self.sB_thValue.setValue(127)

        self.horizontalLayout.addWidget(self.sB_thValue)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.lbl_th_2 = QLabel(self.layoutWidget)
        self.lbl_th_2.setObjectName(u"lbl_th_2")

        self.horizontalLayout_2.addWidget(self.lbl_th_2)

        self.sB_area = QSpinBox(self.layoutWidget)
        self.sB_area.setObjectName(u"sB_area")
        self.sB_area.setAlignment(Qt.AlignCenter)
        self.sB_area.setMinimum(3)
        self.sB_area.setMaximum(1000)
        self.sB_area.setValue(11)

        self.horizontalLayout_2.addWidget(self.sB_area)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.lbl_th_3 = QLabel(self.layoutWidget)
        self.lbl_th_3.setObjectName(u"lbl_th_3")

        self.horizontalLayout_3.addWidget(self.lbl_th_3)

        self.sB_sub_area = QSpinBox(self.layoutWidget)
        self.sB_sub_area.setObjectName(u"sB_sub_area")
        self.sB_sub_area.setAlignment(Qt.AlignCenter)
        self.sB_sub_area.setMaximum(1000)

        self.horizontalLayout_3.addWidget(self.sB_sub_area)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.PlayerFrame = QFrame(self.bgApp)
        self.PlayerFrame.setObjectName(u"PlayerFrame")
        self.PlayerFrame.setGeometry(QRect(210, 25, 571, 551))
        self.PlayerFrame.setFrameShape(QFrame.StyledPanel)
        self.PlayerFrame.setFrameShadow(QFrame.Raised)
        self.gridLayout_6 = QGridLayout(self.PlayerFrame)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.lbl_Player = QLabel(self.PlayerFrame)
        self.lbl_Player.setObjectName(u"lbl_Player")
        font = QFont()
        font.setPointSize(30)
        self.lbl_Player.setFont(font)
        self.lbl_Player.setStyleSheet(u"background-color: rgb(170, 170, 255);")
        self.lbl_Player.setAlignment(Qt.AlignCenter)

        self.gridLayout_6.addWidget(self.lbl_Player, 0, 0, 1, 1)

        self.TranserFrame = QFrame(self.bgApp)
        self.TranserFrame.setObjectName(u"TranserFrame")
        self.TranserFrame.setGeometry(QRect(775, -1, 246, 576))
        self.TranserFrame.setFrameShape(QFrame.StyledPanel)
        self.TranserFrame.setFrameShadow(QFrame.Raised)
        self.groupVideo_Jump = QGroupBox(self.TranserFrame)
        self.groupVideo_Jump.setObjectName(u"groupVideo_Jump")
        self.groupVideo_Jump.setGeometry(QRect(5, 300, 236, 76))
        self.pB_JumpFirstFrame = QPushButton(self.groupVideo_Jump)
        self.pB_JumpFirstFrame.setObjectName(u"pB_JumpFirstFrame")
        self.pB_JumpFirstFrame.setGeometry(QRect(0, 15, 131, 31))
        self.pB_JumpFirstFrame.setCursor(QCursor(Qt.PointingHandCursor))
        self.groupPort = QGroupBox(self.TranserFrame)
        self.groupPort.setObjectName(u"groupPort")
        self.groupPort.setGeometry(QRect(0, 70, 251, 131))
        self.gridLayout = QGridLayout(self.groupPort)
        self.gridLayout.setObjectName(u"gridLayout")
        self.pB_openPort = QPushButton(self.groupPort)
        self.pB_openPort.setObjectName(u"pB_openPort")
        self.pB_openPort.setCursor(QCursor(Qt.PointingHandCursor))

        self.gridLayout.addWidget(self.pB_openPort, 2, 0, 1, 3)

        self.pB_closePort = QPushButton(self.groupPort)
        self.pB_closePort.setObjectName(u"pB_closePort")
        self.pB_closePort.setCursor(QCursor(Qt.PointingHandCursor))

        self.gridLayout.addWidget(self.pB_closePort, 2, 3, 1, 1)

        self.cB_baud = QComboBox(self.groupPort)
        self.cB_baud.setObjectName(u"cB_baud")

        self.gridLayout.addWidget(self.cB_baud, 1, 2, 1, 3)

        self.label = QLabel(self.groupPort)
        self.label.setObjectName(u"label")

        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)

        self.pB_portScan = QPushButton(self.groupPort)
        self.pB_portScan.setObjectName(u"pB_portScan")
        self.pB_portScan.setCursor(QCursor(Qt.PointingHandCursor))

        self.gridLayout.addWidget(self.pB_portScan, 0, 4, 1, 1)

        self.label_2 = QLabel(self.groupPort)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 2)

        self.cB_port = QComboBox(self.groupPort)
        self.cB_port.setObjectName(u"cB_port")

        self.gridLayout.addWidget(self.cB_port, 0, 1, 1, 3)

        self.groupPreview = QGroupBox(self.TranserFrame)
        self.groupPreview.setObjectName(u"groupPreview")
        self.groupPreview.setGeometry(QRect(0, 5, 246, 61))
        self.pB_playerPlay = QPushButton(self.groupPreview)
        self.pB_playerPlay.setObjectName(u"pB_playerPlay")
        self.pB_playerPlay.setGeometry(QRect(10, 20, 191, 33))
        self.pB_playerPlay.setCursor(QCursor(Qt.PointingHandCursor))
        icon1 = QIcon()
        icon1.addFile(u":/btn/res/\u64ad\u653e.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pB_playerPlay.setIcon(icon1)
        self.pB_playerPlay.setIconSize(QSize(25, 25))
        self.pB_stopPlayer = QPushButton(self.groupPreview)
        self.pB_stopPlayer.setObjectName(u"pB_stopPlayer")
        self.pB_stopPlayer.setGeometry(QRect(200, 20, 37, 33))
        self.pB_stopPlayer.setCursor(QCursor(Qt.PointingHandCursor))
        icon2 = QIcon()
        icon2.addFile(u":/btn/res/\u505c\u6b62.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pB_stopPlayer.setIcon(icon2)
        self.pB_stopPlayer.setIconSize(QSize(25, 25))
        self.lbl_info = QLabel(self.TranserFrame)
        self.lbl_info.setObjectName(u"lbl_info")
        self.lbl_info.setGeometry(QRect(10, 380, 226, 181))
        self.groupTranser = QGroupBox(self.TranserFrame)
        self.groupTranser.setObjectName(u"groupTranser")
        self.groupTranser.setGeometry(QRect(0, 200, 246, 96))
        self.pB_transeraframe = QPushButton(self.groupTranser)
        self.pB_transeraframe.setObjectName(u"pB_transeraframe")
        self.pB_transeraframe.setGeometry(QRect(5, 60, 131, 31))
        self.pB_transeraframe.setCursor(QCursor(Qt.PointingHandCursor))
        self.pB_Transfer = QPushButton(self.groupTranser)
        self.pB_Transfer.setObjectName(u"pB_Transfer")
        self.pB_Transfer.setGeometry(QRect(5, 2, 191, 46))
        self.pB_Transfer.setCursor(QCursor(Qt.PointingHandCursor))
        icon3 = QIcon()
        icon3.addFile(u":/btn/res/\u4e0b\u8f7d.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pB_Transfer.setIcon(icon3)
        self.pB_Transfer.setIconSize(QSize(30, 30))
        self.pB_stopTranser = QPushButton(self.groupTranser)
        self.pB_stopTranser.setObjectName(u"pB_stopTranser")
        self.pB_stopTranser.setGeometry(QRect(195, 2, 46, 46))
        self.pB_stopTranser.setCursor(QCursor(Qt.PointingHandCursor))
        self.pB_stopTranser.setIcon(icon2)
        self.pB_stopTranser.setIconSize(QSize(25, 25))
        self.cB_transeraframe_color = QComboBox(self.groupTranser)
        self.cB_transeraframe_color.addItem("")
        self.cB_transeraframe_color.addItem("")
        self.cB_transeraframe_color.setObjectName(u"cB_transeraframe_color")
        self.cB_transeraframe_color.setGeometry(QRect(140, 60, 96, 31))
        MainWindow.setCentralWidget(self.stylesheet)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"toScreen", None))
        self.pB_LoadImageOrVideo.setText(QCoreApplication.translate("MainWindow", u"\u52a0\u8f7d", None))
        self.groupTranserScreen.setTitle(QCoreApplication.translate("MainWindow", u"\u8bbe\u7f6e\u5c4f\u5e55\u5206\u8fa8\u7387", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"\u5bbd\uff1a", None))
        self.sB_Width_Transer.setSuffix("")
        self.sB_Width_Transer.setPrefix("")
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"\u9ad8\uff1a", None))
        self.cB_fillwhite.setText(QCoreApplication.translate("MainWindow", u"\u5bf9\u5927\u5c0f\u4e0d\u8db3\u90e8\u5206\u586b\u5145\u767d(\u9ed8\u8ba4\u9ed1\u8272)", None))
        self.groupProcess.setTitle(QCoreApplication.translate("MainWindow", u"\u56fe\u7247/\u89c6\u9891\u5904\u7406", None))
        self.cB_invertColor.setText(QCoreApplication.translate("MainWindow", u"\u53cd\u8f6c\u989c\u8272", None))
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"\u5bbd\uff1a", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"\u9ad8\uff1a", None))
#if QT_CONFIG(tooltip)
        self.label_6.setToolTip("")
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        self.label_6.setStatusTip("")
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(whatsthis)
        self.label_6.setWhatsThis("")
#endif // QT_CONFIG(whatsthis)
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"\u5e27\u7387", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"\u8df3\u5e27", None))
        self.groupAlgorithm.setTitle(QCoreApplication.translate("MainWindow", u"\u9608\u503c\u5206\u5272\u7b97\u6cd5", None))
        self.cB_threshold.setItemText(0, QCoreApplication.translate("MainWindow", u"Otsu", None))
        self.cB_threshold.setItemText(1, QCoreApplication.translate("MainWindow", u"\u56fa\u5b9a\u9608\u503c", None))
        self.cB_threshold.setItemText(2, QCoreApplication.translate("MainWindow", u"\u5c0f\u533a\u57df\u5747\u503c", None))
        self.cB_threshold.setItemText(3, QCoreApplication.translate("MainWindow", u"\u5c0f\u533a\u57df\u52a0\u6743\u9ad8\u65af\u6838", None))

        self.lbl_th.setText(QCoreApplication.translate("MainWindow", u"\u56fa\u5b9a\u9608\u503c\uff1a", None))
        self.lbl_th_2.setText(QCoreApplication.translate("MainWindow", u"\u5c0f\u533a\u57df\u8fb9\u957f\uff1a", None))
        self.lbl_th_3.setText(QCoreApplication.translate("MainWindow", u"\u5c0f\u533a\u57df\u9608\u503c\u51cf\u503c\uff1a", None))
        self.lbl_Player.setText(QCoreApplication.translate("MainWindow", u"<html><head/><body><p><span style=\" font-size:28pt;\">toScreen</span></p><p><span style=\" font-size:12pt;\">v1.0.0.1</span></p></body></html>", None))
        self.groupVideo_Jump.setTitle(QCoreApplication.translate("MainWindow", u"\u89c6\u9891\u8df3\u8f6c", None))
        self.pB_JumpFirstFrame.setText(QCoreApplication.translate("MainWindow", u"\u8fd4\u56de\u9996\u5e27", None))
        self.groupPort.setTitle(QCoreApplication.translate("MainWindow", u"\u4e32\u53e3", None))
        self.pB_openPort.setText(QCoreApplication.translate("MainWindow", u"\u6253\u5f00\u4e32\u53e3", None))
        self.pB_closePort.setText(QCoreApplication.translate("MainWindow", u"\u5173\u95ed\u4e32\u53e3", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"\u4e32\u53e3\uff1a", None))
        self.pB_portScan.setText(QCoreApplication.translate("MainWindow", u"\u626b\u63cf", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"\u6ce2\u7279\u7387\uff1a", None))
        self.groupPreview.setTitle(QCoreApplication.translate("MainWindow", u"\u9884\u89c8", None))
        self.pB_playerPlay.setText(QCoreApplication.translate("MainWindow", u"\u9884\u89c8\u4f20\u8f93\u7ed3\u679c", None))
        self.pB_stopPlayer.setText("")
        self.lbl_info.setText("")
        self.groupTranser.setTitle("")
        self.pB_transeraframe.setText(QCoreApplication.translate("MainWindow", u"\u4f20\u8f93\u4e00\u6b21", None))
        self.pB_Transfer.setText(QCoreApplication.translate("MainWindow", u"\u5f00\u59cb\u4f20\u8f93", None))
        self.pB_stopTranser.setText("")
        self.cB_transeraframe_color.setItemText(0, QCoreApplication.translate("MainWindow", u"\u5168\u9ed1", None))
        self.cB_transeraframe_color.setItemText(1, QCoreApplication.translate("MainWindow", u"\u5168\u767d", None))

    # retranslateUi

