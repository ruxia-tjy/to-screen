# Config文件配置

```json
{
  "checkUpdate": true,    // 启动检查更新
  "default": {            // 默认值
    "baud": 115200,       // 波特率
    "tran-width": 128,    // 屏幕宽
    "tran-height": 64,    // 屏幕高

    "th": 127,            // 固定阈值的大小
    "side-area": 11,      // 小区域均值的边长
    "sub-area": 0,        // 小区域均值的减值

    "video": {
      "fps": 30,          // 视频帧率
      "fs": 0,            // 跳帧，每几帧取一帧
      "width": 128,       // 缩放的宽
      "height": 64        // 缩放的高
    },
    "image": {
      "width": 128,       // 缩放的宽
      "height": 64        // 缩放的高
    }
  }, 
  "supportExtension": {   // 支持的扩展名
    // 使用opencv处理，但我不知道opencv支持的全部格式，因此列出常用的，可以再次添加
    // gif之类的动图列入Video。
    "Video": [".mp4",".avi",".gif",".flv",".ts"],   // 视频扩展名
    "Image": [".png",".jpg",",jpeg",".bmp"]         // 图片扩展名
  }
}
```